# Projeto HPS Criticidade cirúrgica

### Dados Originais

#### Campo: ```Id```

Código do paciente 

#### Campo: ```MesesEsperaOriginal```

Meses de espera do paciente para a cirurgia - Tempo_espera_meses

#### Campo: ```CriticidadeOriginal```

Criticidade de sucesso original - PatSuc (Patient success)

### Dados engendrados

#### Campo: ```MesesEsperaOriginalEscalonado```

```MesesEsperaOriginal``` escalonado de ```EscalonamentoInicial```, definido como $1$ até ```EscalonamentoFinal```, definido como $5$.

$$MesesEsperaOriginalEscalonado_i = \left (\frac{(EscalonamentoFinal - EscalonamentoInicial)}{\max MesesEsperaOriginal - \min MesesEsperaOriginal} \right ) * \left ( MesesEsperaOriginal_i - \max MesesEsperaOriginal  \right ) + EscalonamentoFinal$$

#### Campo: ```IndiceProposto```

Índice de importância para ordenamento da fila de cirurgias. Quanto maior mais prioritário.

$$IndiceProposto_i = (CriticidadeOriginal_i * MesesEsperaOriginalEscalonado_i)$$

#### Campo: ```SemanaOriginal```

Semada programada para cirurgia, conforme ordenação dos pacientes do maior para o menor ```MesesEsperaOriginal```. Sequencia iniciada em 1 com repetição nos números pares. Exemplo: 1, 2, 2, 3, 4, 4, 5...


#### Campo: ```SemanaProposta```

Semada proposta para cirurgia, conforme ordenação dos pacientes do maior para o menor ```IndiceProposto```. Sequencia iniciada em 1 com repetição nos números pares. Exemplo: 1, 2, 2, 3, 4, 4, 5...

#### Campo: ```DeltaTempo```

Diferenca de tempo entre a ```SemanaProposta``` e ```SemanaOriginal``` convertido para meses (4 semanas).

$$DeltaTempo = \frac{(SemanaProposta - SemanaOriginal)}{4}$$

#### Campo: ```MesesEsperaProposto```

Nova quantidade de meses de espera considerando a semana proposta.

$$MesesEsperaProposto = (MesesEsperaOriginal + DeltaTempo)$$

#### Campo ```MesesEsperaPropostoEscalonado```

```MesesEsperaProposto``` escalonado de ```EscalonamentoInicial```, definido como $1$ até ```EscalonamentoFinal```, definido como $5$.

$$MesesEsperaPropostoEscalonado_i = \left (\frac{(EscalonamentoFinal - EscalonamentoInicial)}{\max MesesEsperaProposto - \min MesesEsperaProposto} \right ) * \left ( MesesEsperaProposto_i - \max MesesEsperaProposto \right ) + EscalonamentoFinal$$

#### Campo ```CriticidadePredicao```

Predicao da criticidade com regressão linear utilizando como preditores:

$$CriticidadePredicao_i = 0 + \beta_1 * MesesEsperaProposto_i + \beta_2 * MesesEsperaProposto_i^2 +  \beta_3 * \sqrt{MesesEsperaProposto_i}$$

#### Campo: ```IndicePropostoPredicao```

Índice de importância para ordenamento da fila de cirurgias. Quanto maior mais prioritário.

$$IndicePropostoPredicao_i = (CriticidadePredicao_i * MesesEsperaPredicaoEscalonado_i)$$


#### Campo: ```DeltaCriticidade```

Diferença entre a criticidade original e a criticidade predita.

$$DeltaCriticidade_i = (CriticidadeOriginal_i - CriticidadePredicao_i)$$.


