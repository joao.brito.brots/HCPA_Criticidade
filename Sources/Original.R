#####
## HPC Criticidade
#

#### Setup ####
source("_Setup.R")

# Lista de modelos
modelos <- Modelos()

#### Processamento ####
dados <- CarregarDadosOriginais() %>% 
  arrange(desc(MesesEsperaOriginal)) %>% 
  mutate(
    MesesEsperaOriginalEscalonado = EscalonarCampo(
      campo = MesesEsperaOriginal,
      escalaInicial = 1L,
      escalaFinal = 5L
    ),
    IndiceProposto = (CriticidadeOriginal * MesesEsperaOriginalEscalonado),
    SemanaOriginal = SequenciaComRepeticaoAlternada(
      n = n(),
      k = 2
    ),
  ) %>% 
  arrange(desc(IndiceProposto)) %>% 
  mutate(
    SemanaProposta = SequenciaComRepeticaoAlternada(
      n = n(),
      k = 2
    ),
    DeltaTempo = ((SemanaProposta - SemanaOriginal) * 0.25),
    MesesEsperaProposto = (MesesEsperaOriginal + DeltaTempo),
    CriticidadePredicao = RegressaoFogliattoOriginal(
      preditor = MesesEsperaProposto
    ),
    MesesEsperaPropostoEscalonado = EscalonarCampo(
      campo = MesesEsperaProposto,
      escalaInicial = 1L,
      escalaFinal = 5L
    ),
    CriticidadePredicao = ifelse(
      test = (MesesEsperaOriginal == MesesEsperaProposto),
      yes = CriticidadeOriginal,
      no = CriticidadePredicao
    ),
    IndicePropostoPredicao = (CriticidadePredicao * MesesEsperaPropostoEscalonado),
    DeltaCriticidade = (CriticidadePredicao - CriticidadeOriginal) 
  ) %>% 
  mutate_if(is.double, round, 2) %>% 
  arrange(desc(IndiceProposto))

#### Resultados ####
openxlsx::write.xlsx(
  x = dados,
  file = "Resultados/Resultados.xlsx",
  asTable = TRUE
)

####
## Fim
#
