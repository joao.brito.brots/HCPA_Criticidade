#' Factor to dummy
#'
#' @param dados 
#' @param campo 
#'
#' @return
#' @export
#'
#' @examples
FactorToDummy <- function(dados, chave, campo, valor = "Dummy", tirarUltimaDummy = FALSE){
  
  # Se for para criar dummys
  if(valor == "Dummy"){
    # Converte factor em dummys 
    dados %<>% 
      select(-!!campo) %>% 
      left_join(
        dados %>% 
          select(!!chave, !!campo) %>% 
          mutate(
            Dummy = TRUE
          ) %>% 
          spread(
            key = !!sym(campo),
            value = Dummy,
            fill = FALSE
          ),
        by = chave
      ) 
    
    # Se deve retirar a ultima dummy
    if(tirarUltimaDummy){
      dados %<>% 
        select(-last_col())
    }
    
  } else {
    # Converte factor em dummys 
    dados %<>% 
      select(-!!campo, -!!valor) %>% 
      left_join(
        dados %>% 
          select(!!chave, !!campo, !!valor) %>% 
          spread(
            key = !!sym(campo),
            value = !!sym(valor),
            fill = 0
          ),
        by = chave
      ) 
  }
  
  # Ajustes finais
  dados %<>% 
    select(-starts_with("<NA>"))
  
  # Retorno da funcao
  return(dados)  
}
####
## Fim
#